#!/usr/bin/python
import subprocess as sb

from subprocess import call 
import os
import socket
import fcntl
import struct

import pdb
from mininet.topo import Topo
from mininet.net import Mininet
from mininet.node import Host
from mininet.log import setLogLevel
from mininet.topo import SingleSwitchTopo
from mininet.util import quietRun,numCores
from sys import exit
from cmd import Cmd
from mininet.node import Controller, OVSSwitch
from mininet.cli import CLI
from mininet.node import Controller,RemoteController
from mininet.util import dumpNodeConnections
from mininet.cli import CLI
from mininet.log import info

def emptyNet():

	net = Mininet()
	
	c0 = net.addController(name='c0',controller=RemoteController,ip='10.0.2.15',port=6633)

	Google = net.addHost('Google',ip="74.125.206.105", defautlRoute="via 75.125.206.105")
	h1 = net.addHost('h1',ip="164.11.203.57", defaultRoute = "via 164.11.203.57")
	h2 = net.addHost('h2',ip="1.1.1.1", defaultRoute = "via 1.1.1.1")

	Facebook = net.addHost('Facebook',ip='31.13.90.38', defaultRoute="via 31.13.90.38")
	s2 = net.addSwitch('s2')
	net.addLink(h1,s2)
	net.addLink(h2,s2)

#	s1 = net.addSwitch('s1')
	net.addLink(Google,s2)
	net.addLink(Facebook,s2)

	net.start()
	
	
	net.pingAll()
#	print h2.cmd('ping -c2 %s'% h1.IP())
	
	for h in [h1, h2]:
		h.sendCmd('echo "" >/root/Desktop/Heure/receptionT.py; python /root/Desktop/Heure/Time.py >> /root/Desktop/Heure/receptionT.py ; echo " " >/root/Desktop/DeType/receptionDt.py; python /root/Desktop/DeType/Type.py >> /root/Desktop/DeType/receptionDt.py ; echo " " >/root/Desktop/Version/receptionV.py ; python /root/Desktop/Version/Version.py >> /root/Desktop/Version/receptionV.py')
	
#	for h in [h1]:
#		h.sendCmd('echo "Ceci est un message de h1"> /root/Desktop/Heure/receptionT.py')
#	for h in [h2]:
#		h.sendCmd('echo "ceci est un message de h2" >> /root/Desktop/Heure/receptionT.py')


	CLI(net)
	net.stop()

if __name__ == '__main__':
	setLogLevel('info')
	emptyNet()
