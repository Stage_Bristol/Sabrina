#!/usr/bin/python

import subprocess

import os
import socket
import fcntl
import struct

import pdb
from mininet.topo import Topo
from mininet.net import Mininet
from mininet.node import Host
from mininet.log import setLogLevel
from mininet.topo import SingleSwitchTopo
from mininet.util import quietRun,numCores
from sys import exit
##################################################CREATION DE LA TOPOLOGIE##############################################
class MyTopo(Topo):
        "exemple de simulation d'apres Mininet custom Topologies sur youtube"
        def __init__(self):
                #Initialisation de la topologie
                Topo.__init__(self)
                #Ajout des noeuds
		h1=self.addHost('h1',ip='10.167.105.67')
		h2=self.addHost('h2',ip='10.167.105.68')
		h3=self.addHost('h3',ip='10.167.105.72')
		h4=self.addHost('h4',ip='10.167.105.73')
		#s0=self.addSwitch('s0')

		#Creation des liens entre chaques noeuds
		#self.addLink(h0,s1)
		#self.addLink(h1,s1)
		#self.addLink(h2,s1)
	def build( self, n=4 ):
		switch = self.addSwitch( 's1' )
		for h in range(n):
	    		# Each host gets 50%/n of system CPU
	    		host = self.addHost( 'h%s' % (h +1), cpu=.5/n )
	    		# 10 Mbps, 5ms delay, 2% loss, 1000 packet queue
	    		self.addLink( host, switch, bw=10, delay='5ms', loss=2, max_queue_size=1000, use_htb=True )


topos = {'mytopo': (lambda: MyTopo())}
################################################AFFICHAGE ADRESSE IP DU CONTROLEUR#######################################
def get_ip_address(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    #packed_data = s.pack(string.encode('utf-8'))
    return socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', ifname[:15])
    )[20:24])

print(get_ip_address('lo'))
print(get_ip_address('eth0'))
