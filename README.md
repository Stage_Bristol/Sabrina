# Bienvenue dans mon Readme Stage à Bristol à l'UWE !
# Welcome to my Readme Stage in Bristol at the UWE !

Ce projet est ouvert dans le but d'expliquer en quoi va consister mon stage, mes differentes taches etc..
This project is open in order to explain what my internship will consist of, my different tasks etc....

## Auteur
## Author
- Sabrina BACO

# Tuteur
# Tutor
- Docteur Arabo

# Professeur référent 
# Lead teacher 
- Monsieur Jones 

## Sujet du stage
## Subject of the internship
SDN - Software Defined Network

### Objectifs du stage
### Objectives of the internship
Chaque semaine de nouveaux objectifs seront donnés par le tuteur 
Each week new objectives will be given by the tutor 

### Semaine du Lundi 09 Avril --> 13 Avril 2018
### Week of Monday April 09 --> April 13, 2018
Répondre aux problématiques suivantes : 
- Comment le SDN s'applique t il à la sécurité informatique en général
- Existe - t - il des outils pour simuler un SDN?

Respond to the following issues: 
- How does the NDS apply to computer security in general?
- Are there tools to simulate a SDN?

### Semaine du Lundi 16 Avril --> 20 Avril 2018
### Monday, April 16 Week --> April 20, 2018

- Creer le planning de la semaine
- Faire des recherches + lectures des documents de Dr. Arabo
- Simuler des scénarios


- Create the schedule of the week
- Researching + reading Dr. Arabo's documents
- Simulate scenarios

### Semaine du Lundi 23 Avril --> vendredi 27 Avril 2018
### Monday April 23rd Week --> Friday April 27th, 2018

- Apprendre à utiliser le logiciel Lyx
- Continuer le rapport
- Simuler un SDN avec les outils choisis

- Learn how to use Lyx software
- Continue report
- Simulate a SDN with the selected tools

### Semaine du Lundi 30 Avril --> vendredi 05 mai 2018
### Monday, April 30 Week --> Friday, May 05, 2018

- Simuler un SDN relié à l'AI de Jean (Simuler un ordinateur déconnecté et le controler + AI reconnaissent qu'il y a un probleme)
- Continuer le rapport

- Simulate a SDN connected to Jean's AI (Simulate a disconnected computer and control it + AI recognize that there is a problem)
- Continue report

### Semaine du Lundi 7 Mai --> vendredi 11 mai 2018
### Monday, May 7 Week --> Friday, May 11, 2018

- Comprendre comment le controlleur reçoit des informations vias le switch
- Trouver une machine virtuelle adaptée à nos besoins
- Continuer le rapport
- Commencer le rapport en Français
- Concrêtiser la simulation controleur + IA 

- Understand how the controller receives information vias the switch
- Find a virtual machine adapted to our needs
- Continue report
- Start the report in French
- Concrete controller + IA simulation 

### Semaine du Lundi 14 Mai --> vendredi 18 mai 2018
### Monday May 14 Week --> Friday May 18, 2018

- Reglé le souci de l'emplacement de la VM pour les libraires python
- Continuer les rapports
- Comprendre comment le controleur reçoit les informations de chaques hotes

- Fixed concern about the location of the VM for python booksellers
- Continue reporting
- Understand how the controller receives information from each host

### Semaine du Lundi 21 Mai --> vendredi 25 mai 2018
### Monday May 21 Week --> Friday May 25, 2018

- Comprendre comment le controleur reçoit les informations de chaques hotes
- Comprendre comment récuperer l'heure de chaque hote
- Continuer les rapports à vitesse grand V !

- Understand how the controller receives information from each host
- Understand how to retrieve the time of each host
- Continue gears at high speed!

### Semaine du Lundi 04 Juin  --> Samedi 09 Juin 2018
### Week of Monday 04 June --> Saturday 09 June 2018

- Récuperer l'heure sur chaque hote
- Récupérer la localisation sur chaque hote
- Comprendre comment faire en sorte que le controleur puisse bloquer des requêtes
- Terminer le rapport brouillon

- Get the time on each host
- Recover the location on each host
- Understand how to ensure that the controller can block requests
- Complete draft report

### Semaine du Lundi 11 Juin  --> Samedi 16 Juin 2018
### Monday, June 11 Week --> Saturday, June 16, 2018

- Récuperer le type d'équipement
- Récuperer l'information de mise à jour ou non
- Terminer le diaporama
- Etre sur d'avoir un rapport PARFAIT !

- Recover type of equipment
- Retrieve the update information or not
- Finish the slide show
- Make sure you get a PERFECT report!

### Semaine du Lundi 18 Juin  --> Samedi 22 Juin 2018
### Monday, June 18 Week --> Saturday, June 22, 2018

- Implémentation de l'heure et de l'adresse IP
- Transformer l'heure et l'adresse IP en variable

- Implementation of time and IP address
- Transform time and IP address into variable

### Semaine du Lundi 25 Juin  --> Samedi 29 Juin 2018
### Monday, June 25 Week --> Saturday, June 29, 2018

- Fin du code de récupération de l'heure, du device type et de l'os update
- Implémentation du tout dans le code de Jean

- End of time, device type and bone update recovery code
- Implementation of the whole in John's code

### Comment s'organise les réunions 
### How meetings are organized 

A chaque fin de semaine, une réunion avec le tuteur aura lieu.
Le tuteur nous demande de lui présenter deux slides.<br>
Slide 1) Ce qu'on a trouvé par rapport aux problematiques proposées<br>
Slide 2) Ce qu'on pense faire plus tard.

Every week, a meeting with the tutor will take place.
The tutor asks us to present two slides.
Slide 1) What we found in relation to the proposed problems<br>
Slide 2) What we think we'll do later.

### Progression 
### Progression 
Voir le planning prévisionnel du lien suivant : https://gitlab.com/Stage_Bristol/Sabrina/tree/master/Planning
<br>
Lire mon Journal de bord
<br>
Jeter un coup d'oeil aux rapports : https://gitlab.com/Stage_Bristol/Sabrina/tree/master/Rapport

See the provisional schedule of the following link: https://gitlab.com/Stage_Bristol/Sabrina/tree/master/Planning
<br>
Read my Logbook
<br>
Take a look at the reports: https://gitlab.com/Stage_Bristol/Sabrina/tree/master/Rapport