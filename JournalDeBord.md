<h2> 09/04/2018</h2>
Bonjour, 
<br>
Cher Journal,
<br>
Ajourd'hui ce fut ma premiere journée de stage. Avec Jean nous sommes arrivée 2h à l'avance pour s'habituer aux lieux.
<br>
Nous nous sommes un peu perdu dans l'université jusqu'à demander notre chemin et trouvé la "Library". Là où nous avons attendu 11h l'heure de notre rdv avec Dr. Arabo.
<br> 
A notre arrivé, Arabo, nous a donné notre sujet et les objectifs à atteindre. En gros chaque semaine il nous donne de nouveaux objectifs.
<br> Ce qui est interessant c'est que la maniere de travailler avec Arabo c'est comme si nous étions notre propre patron. Il n'y a pas d'heure d'arrivée ou de dépzrt c'est à nous de choisir
<br> Enfin voilà, pour le moment avec Jean nous avons fait un planning, on pense bosser de 9h à 18h chaque jours avec 3 jours de travails intensif et un vendredi aprem de visite de Bristol.
<br> A demain ! 
<br>
<h2> 10/04/2018</h2>
Cher journal,
<br>
Aujourd'hui j'ai fait des recherches pendant toute la journée. Des mon arrivé j'ai commencé par chercher ce que c'était que le SDN
<br> En quoi cela consistait. Apres avoir compris son fonctionnement, j'ai fais des schéma et tout sur feuille.. j'ai essayé de trouver le lien entre SDN et sécurité.
<br> C'est difficile de trouver de bon articles à ce sujet puisque la plupart des informations sur le SDN sont surtout lié à comment "cracker" un reseau SDN, ce qui est totalement 
<br> Different de ce que je recherche. Enfin bref, je pense que pour les prochaines séances je vais chercher comment simuler une solution SDN et si possible apprendre 
<br> à gerer l'outil de simulation pour ensuite "creer" mon propre systeme SDN, et mieux comprendre comment la sécurité est importante dans ce domaine.
<br> Aussi j'ai revu un peu le court de Mr Mathieu Hoareau, tres instructif sur la sécurité et cybersecurité, critere DCIP... 
<h2> 11/04/2018</h2>
Cher journal,
<br>
Aujourdhui sans trop mentir j'ai pas trop bossé. Je suis arrivée avec Jean à l'uwe vers 11h, le matin on s'occupait de tout ce qui disait école d'ingé. Ensuite arrivée on a un peu continué <br>
nos recherches et essayé de voir les questions qu'on pourrait poser à Arabo. Vers 14h on a reçu un mail de Arabo nous disant qu'il y avait un meeting du staff et qu'on pouvait y aller si on était <br>
interessé. Le meeting se déroulait à 15h. Nous y sommes allés et c'etait plutot sympathique puisque les personnes autours utilisées des termes techniques, ce qui est un plus pour "improve our language".<br>
A la suite du meeting on a un peu discuté avec Arabo il nous a dit en clair ce qu'il attendait vraiment de nous cette semaine. C a d se renseigner sur le sujet et chercher un outil de simulation <br>
celui qui nous conviendrait le mieux. Du coup en rentrant au boureau j'ai commencé à chercher quelques outils. Le premier sur lequel je suis tombé c'est Matlab. Sans mentir j'ai pas tellement envie 
de faire du Matlab, mais je me dis que ça peut etre interessant puisque Arabo s'y connait et qu'on a deja un peu vu ça en classe. <br>Un autre outil interessant sur lequel je suis tombé c'est Mininet 
j'ai mis la liste de tous les outils sur l'issue #5. Je vais donc tester Mininet et voir ce que cela donne sinon, voir avec Matlab.
<h2> 12/04/2018</h2>
Cher journal,
<br>
Aujourd'hui j'ai un peu avancé sur la presentation que l'on aura demain avec Dr. Arabo. Et j'ai aussi bossé sur un outil de simulation. L'outil en question c'est Mininet et c'est plus un emulateur<br>
qu'un simulateur. Il est programmable avec python. Et en gros il permet d'émuler des hotes, switch et controllers dans le but de simuler une solution SDN. Ca fonctionne assez bien, je l'ai installé sur machine
virtuelle via le site mininet.org et j'ai regardé des petits tutos dessus. Par contre apres avoir simulé la solution SDN je sais pas trop ce que je dois en faire de cet outil. 
Concernant la présentation, j'ai terminé mes deux slides, le premier concerne SDN en lui même, ce que c'est et comment il fonctionne et le second est plus basé sur Mininet.
Je mets les slides sur git histoire de les retrouvé facilement et d'être vu par tous.
<h2> 13/04/2018</h2>
Cher journal,
<br>
Aujourd'hui j'ai terminé les slides pour la réunion avec Dr. Arabo. Je sais à peu pret quoi dire. Dans le 1er slide je vais expliquer ce qu'est le SDN en faisant une comparaison avec un reseau tradionnel,
et dans le 2e slide je vais parler de Mininet l'émulateur d'hote, switch et etc...
Du coup à 16h00 on a eu notre réunion avec Arabo. Elle s'est termine vers 17h. Il a plutot était content du travail qu'on a fait avec Jean. Il a aimé le fait que j'ai utilisé l'outil Mininet et voudrait 
qu'on comment à faire un rapport avec Jean. Du coup je pense on aura 2 rapport. Le futur de notre démarche ce serait de simuler un scénario avec le SDN et que Jean puisse ajouter à mon controller un intelligence artificiel
pour que le reseau soit encore plus intelligent (Monitoring automatique). Il nous a dit qu'il nous publierait lol si cela était vraiment bien et que surtou on était là pour apprendre des choses qu'on aime et s'amuser.
<br><h3>*******************************************************WEEKEND**************************************************************<H3>
<h2> 16/04/2018</h2>
Cher journal,
<br>
Aujourd'hui on est arrivé à l'uwe dans l'apres midi psque le matin j'avais un entretien pour Polytech tours. Ça s'est assez bien passé. Enfin voilà du coup l'apres midi on est allé à l'uwe et on a bossé sur ce que
Arabo nous a donné. On a fait le planning previsionnel de la semaine, ensuite on a commencé notre rapport comme Arabo voulait. On l'a fait en français et en anglais et ensuite (je dis on psque Jean et moi avons fait à peu pret la meme chose)
on a lu les documents que Arabo nous avait rnvoyé. On a pas terminé mais une bonne partie.
<h2> 17/04/2018</h2>
Cher journal,
<br>
Ajourd'hui j'ai continué à lire les documents de M. Arabo. Mais me suis tout aussi documenté sur OpenVSwitch. Si j'ai bien compris c'est un emulateur de switch. En gros ce qu'il faudrait que je fasse c'est liée openVSwitch à Mininet.
Pour le moment je pense continué à faire des recherches dessus et sur les autres logiciels vu avec Mr. Arabo. De plus avec Jean on s'est dit que ce serait interessant de trouver plusieurs scénarios et de faire une analogie au systeme immunitaire 
humain pour chacun. Et voir comment proteger notre reseau de la même maniere que le systeme immunitaire l'est.
<h2> 18/04/2018</h2>
Cher journal,
<br>
Aujourd'hui j'ai un peu plus bossé sur openflow. J'ai vu en détail comment ça fonctionnait. Pour cela j'ai installé une machine virtuelle dans laquelle j'ai installer OpenVSwitch.
Ceci m'a permi de creer un switch virtuel avec des ports virtuels connecté à une machine virtuel qui si j'ai bien compris sera mininet. J'ai aussi traduit une partie d'un des textes que Arabo m'a 
envoyé. Il parlait des attaues DDoS et comment faire en sorte qu'avec SDN on puisse anticiper cela. D'apres ce que j'ai compris. Il faut comparer le comprtement humain et celui d'un robot et 
indiquer à une machine lorsque le comportement ne parait pas humain qu'il ne l'est pas. Cela grace à une "table de hachage".
<h2> 19/04/2018</h2>
Cher journal,
<br>
Aujour'hui j'ai beaucoup étudié le fonctionnement de OpenVSwitch. J'ai simulé tout cela. J'ai de plus appris l'existence de Floodlight qui est un controleur avec OpenFlow integré.
J'ai essayé de le lier à OpenVswitch mais sans succes, je re essaierais demain. J'ai aussi étudié un peu plus ce que c'était un controleur, puisque l'objectif que l'on a avec Jean est de lier
l'IA que Jean a crée avec le controleur. Ainsi, si on ne comprend pas parfaitement ce qu'est qu'un controleur et son fonctionnement comment réussir à creer des scénarios correctes? Je ne sais plus si je l'ai
dis mais avec Jean on s'était dit que cela pourrait être interessant de comparer nos scénarii à ceux du systeme immunitaire un peu comme Arabo l'a fait.
<h2> 20/04/2018</h2>
Cher journal,
<br>
Aujourd'hui j'ai surtout bossé sur la diapo et ce que j'allais dire à Arabo. Dans mon 1er slide j'ai parlé de ce que j'avais vu, openVSwitch, Floodlight et NetFGPA. Jlui ai raconté comment j'avais utilisé ces outils et ce que j'aimerais en faire 
plus tard. Dans la 2e diapo je lui ai parlé de ce que j'aimerais faire plus tard et de EstiNet qui semblait mieux que Mininet. Il était plutot étonné du travail qu'on avait fait avec Jean et qu'il pensait qu'on avait bcp travaillé.
Il nous a aussi montré un nouvel outil appelé texniccenter - c'est comme word mais avec de la programmation et qui permet de faire plus de choses du coup x) 
<br><h3>*******************************************************WEEKEND**************************************************************<H3>
<h2> 23/04/2018</h2>
Cher journal,
<br>
Aujourd'hui on est resté bossé chez Josephine. J'ai fait le rapport toute la journée. En réalité j'ai surtout appris à utiliser Lyx, un équivalent de TecnixCenter sur linux. Ca prend un peu de temps mais 
une fois qu'on a le truc on l'a pour la vie j'imagine.
<h2> 25/04/2018</h2>
Cher journal,
<br>
Hier j'ai completement oublié d'écrire dans mon journal, c'est pourquoi ajd je vais mixer les deux jours. Hier et ajourdh'ui. Hier Jones est venu, il nous a ramené des samoussas franchement c'était cool, ça nous a rappelé des souvenirs. On a un peu discuté avec Arabo et lui. Concernant mon boulot, j'ai totalement récrée une topologie sur python pour mininet. Pour cela, je me suis aidé de plusieurs videos sur utube. Ma topologie est simple, 3 hotes, 1 switch et 1 controller. Avec Jean on s'est dit que pour débuter ce qui serait bien ce serait de faire en sorte que le controleur + l'AI se rendent compte des réseaux connectés et s'il y en a un qui n'est pas co ou s'il y a un intru dans le reseau on peut le detecter. Pour cela, ce que j'ai fais c'est un script avec tcpdump que je lis à mon script python. Puis le tcpdump je le lie à une variable et Jean de son côté essaie de voir quel personne ne devrait pas être connecté sur le reseau par rapport à la variable.
Ce n'est pas entierement terminé mais on espere réussir à faire ça cette semaine.
<h2> 26/04/2018</h2>
Cher journal,
<br>
Aujourd'hui j'ai encore bossé sur la partie topologie avec mon fichier python. En gros j'ai réussi à rediriger mon tcpdump dans une variable qui sera redirégé vers mon fichier python. J'ai pas encore reflechis à comment récuprer 
une adresse une par une et aussi autre soucis lorsque je ping mon switch par exemple je ne vois pas le traffic du ping avec mon tcpdump c'est à revoir ça aussi.
<h2> 27/04/2018</h2>
Cher journal,
<br>
Aujourd'hui j'ai bossé encore sur mon fichier python. J'suis un peu bloqué psque j'arrive pas à faire en sorte que mon réseau analyse les hotes qu'il y a autour. J'arrive à faire mon tcpdump et à analyser le rseau mais j'arrive pas à modifier 
cela pour tous les hotes du reseau. Exemple, lorsque je fais des ping entre 2 hotes bah mon "controleur" ne le vois pas alros qu'il devrait. Bref, je regle ce soucis la semaine prochaine et avec Jean on fait en sorte que de pouvoir lier son IA
et mon réseau simulé.
<br><h3>*******************************************************WEEKEND**************************************************************<H3>
<h2> 30/04/2018</h2>
Cher journal,
<br>
Aujourd'hui sans trop mentir je n'ai pas vraiment bossé. En effet, on a reçu une nouvelle de M. Faucon nous indiquons un compte rendu à rendre au plus vite. J'ai donc fais le compte rendu une bonne grosse partie de l'apres midi
et l'autre partie je me suis attelé à chercher des associations interessantes pour lesquelles je pourrais postuler à l'uwe à l'union student.
<h2> 01/05/2018</h2>
Cher journal,
<br>
Aujourd'hui j'ai travaillé sur la continuité du rapport.
<h2> 02/05/2018</h2>
Cher journal,
<br>
Aujourd'hui j'ai bossé sur mon fichier python en essayant de le fusionner avec Jean. Mais je me rend compte que c'est pas possible enfin ça bloque au niveau du controleur. J'arrive pas à comprendre comment le SDN réussi à voir la topologie entiere.
Normalment c'est grace au switch qui lui donne les informations des hotes connectés. Mais comment faire en sorte que le switch lui donne ces informations pour les récuperer?
Lorsque je suis dans mininet j'arrive à recuperer les informations de tous les hotes mais c'est psque Mininet c'est une interfaces regroupant le tout. Mais lorsque j'essaie de recupere les information depuis le switch
nada. Même avec le tcpdump le traffic ne passe pas. Faudrait que je vois comment faire avec une architecture pseudo reelle lol c'est à dire de vrais machines virtuelle relié à un switch virtuel crée sur mon pc ou alors continuer avec floodlight
qui possède deja des hotes fictifs. Enfin bref je sais pas trop je suis un peu perdu pour le coup.
<h2> 03/05/2018</h2>
Cher journal,
<br>
Aujourd'hui j'ai essayé de comprendre comment le controller recevait les informations des ordinateurs. En fait il passe par le switch grace à opvSwitch, ça j'avais compris mais apparement il utilise de plus le protocole lldp qui lui permet d'avoir 
l'architecture global du reseau. Bon pour le teste avec Jean du coup ce que j'ai fais c'est que je me suis rendu compte qu'il y avait des interfaces qui se crée sur le conroller à chaque fois que je crée des hotes dans ma topologie.
Donc ce que j'ai fais c'est donner une adresse statique à ces interfaces et faire en sorte que Jean récupere la partie hote de l'adress IP, pour qu'il puisse l'analyser.
On a essayé de fusionner nos deux codes ensemble, cependant pour le moment nous avons un probleme de librairie à regler.
<h2> 04/05/2018</h2>
Cher journal,
<br>
Aujourd'hui j'ai surtout préparé la présentation pour la réunion avec Dr. Arabo. J'ai fais les diapos et tous.. De plus, j'ai fais des recherches sur comment intégrer une topologie avec python d'une autre maniere que celle que je connais 
car j'ai remarqué que lorsque je crée des hotes virtuels sur mon switch des interfaces virtuelles se créent tout aussi en foncton du nombre d'hote que j'ai. Cepenant ces interfaces ne sont pas celles des hotes crées. J'aimerais bien comprendre à quoi
servent ces interfaces crées. Pour le teste de Jean ce sont ces interfaces là que j'ai utilisé.
<br><h3>*******************************************************WEEKEND**************************************************************<H3>
<h2> 07/05/2018</h2>
Cher journal,
<br>
Aurjoud'hui fut un jour férié. Nous ne sommes donc pas allé à l'UWE avec Jean. J'ai tout de même un peu continué le rapport pour ne pas prendre de retard.
<h2> 08/05/2018</h2>
Cher journal,
<br>
Aujourd'hui j'ai surtout bosser sur comment faire en sorte d'augmenter la taille de ma VM. Psque si je n'arrive pas à telecharger les libraires c'est notamment parce que je n'ai plus de place dans la VM Floodlight.
J'ai du supprimer la VM la réinstaller à plusieur reprises pour faire des testes et encore je ne suis toujours pas parvenu à atteindre mon but. Enfin j'ai trouvé un logiciel qui apparemment permettait de faire cela. Demain je vais essayer de l'installer et puis si tou se passe bien 
cela devrait fonctionner correctement et je pourrais continuer à avancer. Mr. Arabo est venu nous voir ajd, il nous a dit qu'il serait interessant de pouvoir connaitre la localisation ainsi que l'heure et l'adresse IP des hotes sur lesquels ont travail.
On voir comment nous pourrons faire cela.
<h2> 09/05/2018</h2>
Cher journal,
<br>
Aujourd'hui j'ai essayé d'installer Hyper-V sur windows, en sachant que mon PC a besoin de nombreuse mise à jour cela a pris beaucoup plus de temps que prévu pour au final ne pas fonctionner lol. J'ai commencé à faire des recherches sur 
comment le controleur reçoit les informations du switch grace à OpenFlow et j'ai aussi commencé à tester la technique du cd pour augmenter la taille de la partition. C'est marant, ils vendent des CDs vierges au student Union.
<h2> 10/05/2018</h2>
Cher journal,
<br>
Aujourd'hui j'ai bossé sur le repartionnement de la vm avec le cd que j'avais acheté. Alors d'apres ce que j'ai compris, il faut un cd pour transferer l'emplacement du cd et l'ajouter à la taille de la machine virtuelle. Sauf que le cd que j'ai pu acheter ne fait seulement que 
1mb ce qui n'est pas suffisant il me faut un cd plus grand. J'ai alors essayé d'installer VmWare pour voir si c'était pareil mais en vain, c'est la même chose.<br>
J'ai aussi commencé à travailler sur mon rapport en Français. Je sais ce que je vais dire pour ma 1er partie et j'ai déja fais la mise en page de la page de garde. J'ai deux pages de gardes mais en réalité aucunes des deux ne m'interessent.
Je vais certainement en faire une autre qui me satisfairera mais j'ai surtout envie d'un truc original qui sort de l'ordinaire mais pas trop extravaguant non plus. Enfin voilà je vais y reflechir..
<h2> 11/05/2018</h2>
Cher journal,
<br>
Aurjourd'hui je me suis surtout occupé à bossé sur les slides pour le meeting avec Dr. Arabo. Slides que je trouve plutot réussis Ahah. Enfin voilà j'ai compris un peu plus de choses sur ce que je dois faire et pour le coup j'hésite à changer de controleur ou pas. En effet,
celui que j'utilise Floodlight est basé sur Java or le code qu'utilise Jean est basé sur du Python il serait dont plus judicieux d'utiliser un controleur Python pour fusionner notre code ensemble. C'est pourquoi je pense utiliser le controleur POX qui est entierement basé sur du python. Il est déja préinstallé
sur Mininet mais je pense qu'il est préférable de voir si je peux trouver une VM POX et continuer d'utiliser Floodlight comme VM de simulation puisque sur celle-ci je peux télécharger les paquets necessaires pour l'instalation des librairies Python contrairement à celle de Mininet.
Dr. Arabo est censé me ramener un disque dur lundi pour que je puisse agrandir ma VM mais il est fort probable que lundi je ne sois pas là :o .
<br><h3>*******************************************************WEEKEND**************************************************************<H3>
<h2> 15/05/2018</h2>
Cher journal,
<br>
Ces deux derniers jours je n'ai pas beaucoup travaillé à part le compte rendu. En effet, je suis allé au mariage d'une personne de ma famille ce qui fait que je n'ai pas eu le temps de faire grand chose.
<h2> 16/05/2018</h2>
Cher journal,
<br>
Aujourd'hui avec Jean on est resté travaillé à la maison. J'ai pu récuperer le disque dur d'Arabo pour pouvoir modifier la taille de la machine virtuelle. et en  fait je me suis rendu compte qu'il n'y avait ni besoin de disque dur ni besoin de CD. Il fallait "juste" modifier la vmdk en vdi puis modifier la taille de la vdi
et ensuite re convertir la vdi en vmdk, puis installer gparted sur la VM pour ensuite récuperer la taille ajouté et l'importer. J'ai utilisé le tuto suivant : https://www.youtube.com/watch?v=cDgUwWkvuIY qui m'a franchement beaucoup aidé. Enfin voilà, à la fin de la journée j'ai eu ma VM avec 23 Go de plus :D !
<h2> 17/05/2018</h2>
Cher journal,
<br>
Aujourd'hui, ayant une VM fonctionelle avec la taille necessaire, j'ai installé toutes les libraires dont on avait besoin. Cela m'a pris facile toute la matinée notamment pour tensorflow. Apres cela avec Jean on a pu tester notre codé fusionné. Apres avoir réglé quelques erreurs cela a fonctionné youhou !
Maintenant il me reste à voir le plus important. A la derniere réunion avec Arabo ils nous a donné les informations que je devais donné à Jean pour l'AI. C'est à dire l'heure, la position, et l'adresse IP du PC. Et je n'arrive toujours pas à savoir comment le controlleur voit toute la topologie.
J'ai commencé à voir des tutos. Mais mon controleur Floodlight se programmant en Java je me demande si je ne devrais pas me concentrer sur POX et continuer mes tutos là dessus. Demain réunion avec Dr. Arabo à voir ce que je vais vraiment pouvoir lui présenter, je reste perplexe, j'ai l'impression
de ne pas trop avancer et surtout de stagner mais pour sur la semaine prochaine tout cela va changer et je vais trouver une solutions à toutes les questions que je me pose ! 
<h2> 18/05/2018</h2>
Cher journal,
<br>
Aujourd'hui j'ai surtout travaillé sur la préparation du meeting avec Arabo. Je lui ai montré comment récuperer les pseudos adresses IP de chaque interface pour ensuite les renvoyer aux code de Jean. Tout cela a tres bien fonctionné. Et du coup la semaine prochaine il faudrait pouvir récuperer l'heure de chaque hote et si possible la localisation mais c'est implicite à l'heure,
en fonction de l'UTC.
<br><h3>*******************************************************WEEKEND**************************************************************<H3>
<h2> 21/05/2018</h2>
Cher journal,
<br>
Aujourd'hui j'ai fais 100% rapport. Celui pour Dr. Arabo et celui pour la soutenance, je connais mon plan et sais à peu pret comment l'agencer et ce que je dois mettre dedans pour chaque partie. Je pense que je vais faire exactement le même rapport en français et en anglais en fait. J'ai vu avec M Jones et du coup pour la définition complète du SDN il est préférable de la mettre 
dans l'annexe ce que je pense faire. J'ai aussi passé l'épreuve écrite pour l'IMT de Lille mais voilà ça n'a pas vraiment de rapport avec le stage ahah.
<h2> 22/05/2018</h2>
Cher journal,
<br>
Aujourd'hui j'ai bossé sur comment faire pour récuperer l'heure de chaque hote. Et je trouve pas beaucoup d'aide sur le net. Je comprends bien qu'il faut passer par le controleur mais toutes les configurations que je fais par rapport au controleur sont liées aux reseau, à l'adresse mac et l'adresse ip par exemple.
Mais comment récuperer les données concrête d'un pc rattaché au switch qui lui même est rattaché au controleur? Dois-je passer pa le switch ou il existe un moyen direct à partir du controleur?
Je médite là-dessus. 
<h2> 23/05/2018</h2>
Cher journal,
<br>
Aujourd'hui j'ai fais 50% rapport de stage et 50% écoles d'ingé.. J'ai reçu une réponse positive pour l'ENSIBS école à laquelle je souhaite fortement entrer. Cependant c'est une école en alternance, ce qui signifie qu'à côté y'a la partie recherche d'entreprise qui me prend tout aussi beaucoup de temps.
Je dois ainsi diviser mon temps par trois entre la recherche d'entreprise, les rapports et le stage en lui-même. Bref, les jours s'accelere et ça va aller de plus en plus vite. Mais tout devrais bien se passer :) 
<h2> 24/05/2018</h2>
Cher journal,
<br>
Aujourdh'ui j'ai bossé sur l'heure. Alors je me suis dis en sachant que je sais comment récuperer les adresses ip de chaque hote grace au controleur et à un certain script nommé l3_learning du controleur. Je me suis dis que ce serait interessant de récuperer l'heure à laquelle le controleur voit l'adresse IP de chaque hote. J'ai donc rajouté 
une variable date au moment où le controleur récuperait les adresses IP sur sa console. Cependant j'ai bien peur que l'heure qui s'affiche n'est en réalité  que l'heure du controleur en lui-meme et non pas celui des hotes mais la sienne. Je vais faire des tests et reflechir là-dessus.
<br><h3>*******************************************************BREAK**************************************************************<H3>
<h2> 04/06/2018</h2>
Cher journal,
<br>
Aujourd'hui reprise. En effet, pendant cinq jours nous étions à Londre. Ce fut de petits jours de vacances et avant les cinq jours de Londre le frere et la soeur de Jean étaient à Bristol. Je n'ai donc pas bcp avancé sur le projet en lui-même.
Dernierement avant de partir à Londre nous avions eu une Réunion avec Dr. Arabo, que nous ne reverrons plus avant le 18 juillet soit un jour avant la soutenance. En effet, il part au Qatar pendant 2 semaines.
On a donc du lui founir un planning des prochaines séances ( à telecharger ici --> 8554d9f6ea49ad75a75e0fd0251a56e8ec73d9c2 - ou à voir dans le dossier planning). Mais vu qu'on a pas beaucoup avancé ces derniers temps il va vraiment falloir booster pour tout rattraper. C'est pourquoi le weekend on pense venir à l'UWE. Pour avoir un bon rythme de travail.
C'est bientôt la fin du stage et ça se sent et la pression commence à être là. 
<h2> 10/06/2018</h2>
Cher journal,
<br>
Cela va faire à peu pret une semaine que je n'ai pas ecris de suivis. Mais pour faire simple, cela fait une semaine que j'essaie de trouver un moyen de récuperer l'heure de chaque hote. Ma premiere pensée est de modifier le fichier l3_learning, celui qui joue le role de switch et de suivis des informations. Grace à ce fichier j'ai réussi à récuperer les adresse IP qui circulent sur le reseau et les adresses mac aussi.
Ainsi cela me permet d'éviter d'utiliser la techinique "bourrin" qui consistait à attribuer une adresse IP à chaque poste. Enfin voilà et du coup je me suis dis que ce serait interessant de récuperer l'heure à laquelle un ping a été effectuer sur chaque PC pour pouvoir récuperer l'heure de chaque hote. Mais je dois avouer que je suis un peu bloqué sur cela. Mon autre idéee est d'essayer d'envoyer les information de l'heure depuis les postes au controleur qui les récupere et les stockes dans une variable.
Mais là encore je ne vois pas comment faire pour envoyer des informations au controleur. Je médite là-dessus depuis pret d'une semaine...
<br> Faut juste pas oublier la phrase de Marc Aurel qui dit : <b>"Si tu es bloqué, c'est que tu es sur le bon chemin"</b> <br>
Et à côté de cela, il y a préparation du rapport et de la soutenance..
<h2> 10/06/2018</h2>
Cher journal,
<br>
Aujourd'hui j'ai bossé sur la récuperation de l'heure et je me suis dis puisque je n'arrive pas à la récuperer depuis le controleur autant la récuperer depuis chaque PC, puis l'envoyer au controleur. Je sais comment la récuperer depuis chaque PC mais ne sait pas encore comment l'envoyer au contrôleur. Mais je vais reflechir là dessus.
De plus, je me suis dit que ce serait plus interessant si j'écrivais moi-même mon programme qui joue le rôle de switch pou récuperer les adresses IP et MAC.
<h2> 10/06/2018</h2>
Cher journal,
<br>
Aujourd'hui j'ai bossé et sur l'heure et sur la localisation des adresses IP. Deja j'ai ecrit plusieurs script permettant de récuperer l'heure de chaque hote. Un premier qui execute mon script sh, un..
<h2> 15/06/2018</h2>
Cher journal,
<br>
Cela va faire un petit bout de temps que je n'ai pas écris et pour cause, le rapport. Aujourd'hui c'est le dernier délai pour déposer le rapport. J'ai terminé celui en français et celui en anglais tout aussi. Je vais pas tarder à l'envoyer, je vérifie tout de même pour voir s'il n'y a pas de modifications à faire.
Je sais qu'il ne pourra être parfait mais j'ai tendance à le vouloir.
<br><h3>*******************************************************WEEKEND**************************************************************<H3>
<h2> 19/06/2018</h2>
Cher journal,
<br>
Her et avant hier on a bossé sur la soutenance. On  fait les slides vu avec M. Arabo ce qu'il fallait ajouter... Et passé notre soutenance. Ça s'est plutot bien passé.
Ah et M. Jones voudrait qu'on fasse une video avec Jean concernant notre stage en Angleterre, c'est une bonne idée !
<h2> 20/06/2018</h2>
Cher journal,
<br>
Aujourd'hui j'ai bossé sur les adresse IP. J'avais deja terminé mais je trouvais que mon code n'était pas assez propre. Du coup j'ai un peu tout modifié. Pour récuperer les adresses IP ça marche de cette façon:
Je lance un ping automatiquement depuis le script de ma topologie "testTopo2.py" . En parallèle je lance mon controleur POX avec mon script l3_learning2.py que j'ai modifié pour transferer mes adresses IP sources vers un fichier de reception.
Ce fichier de reception "reception.py" supprime les doublons et stock les adresses IP dans dans variables.
J'ai aussi un peu réfléchis à comment faire pour récuperer l'heure sous python. Avant je le faisais avec Shell mais honnetement autant tout faire sous python. Bref du coup j'ai utilisé la librairie time sous python et crée un programme qui donne l'heure. Je pense 
que je vais faire en sorte quàa chaque fois qu'une simulation est lancée ce programme se lance sur tous les hotes et renvoie l'heure dans un fichier de reception sous forme de variable comme l'adresse IP.
<h2> 21/06/2018</h2>
Cher journal,
<br>
Aujourd'hui j'ai je me suis concentré sur l'heure. J'ai un peu passé du temps sur l'adresse IP encore parce que je m'étais un peu trompé dans mon code. Enfin une fois que tout était fonctionnel je suis passé sur l'heure. Donc comme je l'ai dit j'ai essayé de faire en sorte que mon programme d'heure s'éxecute lorsque je lance ma simulation.
Cependant j'ai eu bcp de mal avec cela. Pourquoi? Parce que ma premiere topologie n'était pas apte à faire cela ou du moins je n'ai pas trouve de solution pour et puis j'ai reflechis et j'ai pensé à la phrase de Steve <b>"En faisant des choses non fonctionnels vous n'obtiendrez que des disfonctionnement" </b>. Alors j'ai reflechis à une autre maniere de faire.
Et ainsi au lieu de lancer ma simulation avec mn --custom ... je la lance avec python -E. Sans le -E il arrive pas à trouver les librairies de Mininet j'ai pas bien compris pourquoi. Mais dans tous le cas avec cette commande j'ai réussi à faire en sorte qu'un hote me donne son heure et la transfere dans un fichier de reception.
Demain je vais essayer de gerer plusieurs hotes à la fois. Ah et aussi avec Jean on a essayé de lier nos code pour la localisation. C'est presque fonctionnel, il y a juste eu des petits problemes d'indentations que l'on devrait tres vite régler.
Demain il y a les résultat pour le DUT j'ai bien hâte !! 
<h2> 22/06/2018</h2>
Cher journal,
<br>
Aujourd'hui j'ai un peu bossé sur le device type je sais comment faire maintenant il me reste plus qu'à faire en sorte que la récupération se fasse sur tous les poste et ensuite le mettre sous forme de variable pour Jean. J'ai continué un peu sur l'heure mais n'ais pas terminé. <br> Et concernant les résultat je suis officielement diplomée :D ! 
<br><h3>*******************************************************WEEKEND**************************************************************<H3>
<h2> 25/06/2018</h2>
Cher journal,
<br>
Aujourdh'ui j'ai réussi a creer un tableau qui récupère toutes les informations necessaires au traitement, il ne me reste plus qu'à enlever le 0 pour l'heure pour Jean car il ne peut pas traier des heure en mode par exemple "08h" mais plutot "8h".
Normalement demain on teste de tout implémenter et si on a le temps on va aussi monitorer l'activiter de l'utilisateur.
<h2> 26/06/2018</h2>
Cher journal,
<br>
Aurjourd'hui j'ai travaillé sur la récupération de toutes les informations. J'ai essayé d'enlever le 0 pour l'heure mais n'est pas encore réussie. Jean veut une autre disposition de ma liste du coup j'ai un peu bossé sur ça ausi.
<h2> 27/06/2018</h2>
Cher journal,
<br>
Aujourdh'ui avec Jean on a bossé sur la fusion de nos de codes. Il y a des petits soucis de d'indentation et de boucle que l'on va arranger au plus vite !
<h2> 28/06/2018</h2>
Cher journal,
<br>
Aujourd'hui avec Jean on a bossé sur la fusion est tout est fonctionnel et nivkel ! Cependant il nous reste l'activité a faire vu qu'on a un peu de temps devant nous pour cela je vais faire en sorte qu'un hote puisse avoir acces à internet et que le controleur  s'apercoive de cette activité. Je récupere ensuite via le controleur l'URL sur lequel la personne a essayé d'aller.
<h2> 29/06/2018</h2>
Cher journal,
<br>
Ajd on a terminé nos simulation et on s'est préparé pour la derniere réunion "pro" avec Arabo x)
<h2> 02/06/2018</h2>
Cher journal,
<br>
Ajd = Rédaction du rapport 