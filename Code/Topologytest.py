#!/usr/bin/env python
# -*- coding: utf-8 -*-

from mininet.net import Mininet
from mininet.node import Controller, OVSSwitch, RemoteController, Host
from mininet.cli import CLI

def emptyNet():

net = Mininet()
	
	c0 = net.addController(name='c0',controller=RemoteController,ip='10.0.2.15',port=6633)

	h1 = net.addHost('h1',ip="164.11.203.57", defaultRoute = "via 164.11.203.57")
	h2 = net.addHost('h2',ip="1.1.1.1", defaultRoute = "via 1.1.1.1")
    
    net.addLink(h1,s2)
    
    net.start()
    
    net.pingAll()
    
    CLI(net)
	net.stop()

if __name__ == '__main__':
	setLogLevel('info')
	emptyNet()
